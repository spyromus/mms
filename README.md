# Minimal Microservices

It's a (mostly fun) effort to create minimum-sized docker containers for microservices in different languages / frameworks.

## Leaderboard

| Container size (Mb) | Language      | Framework / Server              |
|                 --: | :--           | :--                             |
| 63.5                | Python 3.10.4 | Falcon 3.1.0                    |
| 69.2                | Ruby 3.1.1    | Webrick                         |
| 123                 | Python 3.10.4 | FastAPI 0.75.0 + Pydantic 1.9.0 |

## Contributing

- If you have an idea how to lower the size of the container, please submit merge request
- If you want to try some other language / framework / server, please submit merge request. At the very minimum the resulting container should respond to a GET request to any path with code 200 and a message.

Have fun!