import falcon
import falcon.asgi


class Hello:
    async def on_get(self, req, resp):
        """Handles GET."""
        resp.status = falcon.HTTP_200
        resp.content_type = falcon.MEDIA_TEXT
        resp.text = "Hello"


app = falcon.asgi.App()

app.add_route("/hello", Hello())
